#!/bin/bash
# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -e

REPO_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"/..
readonly REPO_ROOT="${REPO_ROOT}"

readonly MANIFEST_DIRECTORY="manifests"

help() {
    echo
    echo "Script used to update all lockfiles in this repository"
    echo
    echo "Usage:"
    echo "   $(basename "$0") [<options>]"
    echo
    echo "Options:"
    echo
    echo "  -h"
    echo "     Prints this help message"
    echo
}

update_ensure_files_in_dir() {
    local cipd_client=$1
    local manifest_dir=$2
    echo "Updating manifest files in ${manifest_dir}"
    cd "${manifest_dir}"

    for file in *.ensure; do
        echo "Running cipd ensure-file resolve on ${file}"
        "${cipd_client}" ensure-file-resolve -ensure-file "${file}"
    done
}

main() {
    while getopts ":h" opt; do
        case ${opt} in
        'h' | '?')
            help
            exit 1
            ;;
        esac
    done

    cd "${REPO_ROOT}"

    if command -v 'cipd' >/dev/null; then
        CIPD_CLIENT="cipd"
    elif command -v '.cipd_client' >/dev/null; then
        CIPD_CLIENT=".cipd_client"
    else
        echo "Cannot find a cipd client"
        exit 1
    fi

    update_ensure_files_in_dir $CIPD_CLIENT "${MANIFEST_DIRECTORY}"
}

main "$@"
